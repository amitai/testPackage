package testPackage

import "fmt"

type Dancer string

func (dancer *Dancer) Dance() {
	fmt.Println(*dancer)
}
